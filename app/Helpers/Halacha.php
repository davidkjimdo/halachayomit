<?php
namespace App\Helpers;

class Halacha {
    public function getHalacha() {
        $client = new \simplehtmldom\HtmlWeb();
        $html = $client->load('http://halachayomit.co.il/he/default.aspx');

        // Returns the page title
        return $html->find('#ctl00_ContentPlaceHolderMain_lblHalachaText', 0)->text();
    }

    public function send() {
        \Telegram\Bot\Laravel\Facades\Telegram::sendMessage([
            'chat_id'    => '-1001255240180',
            'text'       => $this->getHalacha(),
            'parse_mode' => 'HTML',
        ]);
    }

}
